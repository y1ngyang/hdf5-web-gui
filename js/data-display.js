/*global $*/
'use strict';


// External libraries
var AJAX_SPINNER, Plotly, HANDLE_DATASET,

    // Global variables
    DATA_DISPLAY =
    {
        plotCanvasDiv : document.getElementById('plotCanvasDiv'),
        plotExists : false,
        colorScale : 'Picnic',
        colorProfile : 'rgb(102,0,0)',
        plotLogValues : false,
        plotAutoLimits : true,
        plotType : 'heatmap',
        plotDimension : 2,
        displayType : '',

        dataValues : [],
        initialDataValues : [],
        logOfDataValues : [],
        initialDataValuesBPR : [],
        logOfDataValuesBPR : [],
        badPixelsExist : false,

        stringDataValues : [],
        resizeTimer : undefined,
        plotWidth : 550,
        plotHeight : 550,
        useDarkTheme : false,
        mobileDisplay : false,

        imageSeries : false,
        imageSeriesRange : 0,
        imageSeriesIndex : false,
        imageTargetUrl : undefined,
        imageShapeDims : undefined,
        imageIsDownsampled : false,
        imageZoomSection : false,
        usingOriginalImage : true,
        imageTitle : 'Title goes here',

        lineValues : [],
        lineTitle : '',

        loadedImageRange : undefined,
        loadedImageRangeSize : [0, 0],
        loadedImageSize : undefined,

        plotAspect : false,

        // Clear the plotting canvas along with whatever objects were there
        purgePlotCanvas : function () {
            Plotly.purge(DATA_DISPLAY.plotCanvasDiv);
        },


        showPlotCanvas : function () {
            document.getElementById("plotCanvasDiv").style.display = "block";
            document.getElementById("welcomeDiv").style.display = "none";
        },


        // Enable or disable various image and image series controls
        enableImagePlotControls : function (enablePlotControls,
            enableImageControls, enableSeriesControls) {

            var i, debug = false, seriesMax = 0, endButtonWidth = '50px',
                // plotControlDiv = [ '#plotControlDownload',
                //     '#plotControlReset'],
                plotControlDiv = [ '#plotControlDownload'],
                imageControlDiv = ['#plotControlType', '#plotControlLog',
                    '#plotControlColor', '#plotControlAspect',
                    '#plotControlAutoImage'],
                imageControlDivMobile = ['#plot-control-mobile'],
                seriesControlDiv = ['#imageSeriesControl'];

            // Show or hide the automatic intensity limits button -
            // plotControlAutoImage
            if (!DATA_DISPLAY.badPixelsExist) {
                imageControlDiv = ['#plotControlType', '#plotControlAspect',
                    '#plotControlColor', '#plotControlLog'];
            }

            // Show or hide square pixel option
            if (DATA_DISPLAY.plotDimension === 3) {
                imageControlDiv = ['#plotControlType', '#plotControlLog',
                    '#plotControlColor', '#plotControlAutoImage'];
            }

            // General plot controls - show, hide
            if (!DATA_DISPLAY.mobileDisplay) {
                for (i = 0; i < plotControlDiv.length; i += 1) {
                    if (enablePlotControls) {
                        $(plotControlDiv[i]).show();
                    } else {
                        $(plotControlDiv[i]).hide();
                    }
                }
            }

            // General image controls - show, hide
            if (DATA_DISPLAY.mobileDisplay) {
                for (i = 0; i < imageControlDivMobile.length; i += 1) {
                    if (enableImageControls) {
                        $(imageControlDivMobile[i]).show();
                    } else {
                        $(imageControlDivMobile[i]).hide();
                    }
                }
            } else {
                for (i = 0; i < imageControlDiv.length; i += 1) {
                    if (enableImageControls) {
                        $(imageControlDiv[i]).show();
                    } else {
                        $(imageControlDiv[i]).hide();
                    }
                }
            }

            // Image series controls - show, hide
            for (i = 0; i < seriesControlDiv.length; i += 1) {
                if (enableSeriesControls) {
                    $(seriesControlDiv[i]).show();
                } else {
                    $(seriesControlDiv[i]).hide();
                }
            }

            DATA_DISPLAY.imageSeries = enableSeriesControls;

            if (enableSeriesControls) {

                seriesMax = DATA_DISPLAY.imageSeriesRange - 1;

                // Reset the value and limits of the input field
                $("#inputNumberDiv").val(DATA_DISPLAY.imageSeriesIndex);
                $('#inputNumberDiv').attr({
                    'min' : 0,
                    'max' : seriesMax,
                });

                $("#imageSeriesSlider").val(DATA_DISPLAY.imageSeriesIndex);
                $('#imageSeriesSlider').attr({
                    'min' : 0,
                    'max' : seriesMax,
                });

                // Set the text of the start and end buttons
                $('#startButtonValue').text(0);
                $('#endButtonValue').text(' ' + seriesMax);

                // Set the width of the end button, depending on text size
                endButtonWidth = seriesMax.toString().length * 10 + 40;
                endButtonWidth = parseInt(endButtonWidth, 10) + 'px';

                if (debug) {
                    console.log('endButtonWidth: ' + endButtonWidth);
                }

                $('#endButton').css("width", endButtonWidth);
            }
        },


        // Display text
        cleanupStringForDisplay : function (inputString) {

            var debug = false, outputArray, outputString, len, curr, prev,
                element;


            if (debug) {
                console.log('inputString: ' + inputString);
            }

            // Convert to strings, remove bad, bad things
            inputString = String(inputString);
            inputString = inputString.replace(/\$/g, '');

            // Check for empty values
            if (inputString === '') {
                inputString = '<empty value>';
            }

            // Remove leading, trailing, and multiple spaces
            inputString = inputString.replace(/ +(?= )/g, '');
            inputString = inputString.trim();

            // For long strings, split em up
            if (inputString.length > 49) {
                len = 39;
                curr = len;
                prev = 0;

                outputArray = [];

                while (inputString[curr]) {
                    curr += 1;
                    if (inputString[curr] === ' ') {

                        element = inputString.substring(prev, curr);

                        // Remove leading, trailing, and multiple spaces
                        element = element.replace(/ +(?= )/g, '');
                        element = element.trim();

                        outputArray.push(element);

                        prev = curr;
                        curr += len;
                    }
                }

                element = inputString.substring(prev);

                // Remove leading, trailing, and multiple spaces
                element = element.replace(/ +(?= )/g, '');
                element = element.trim();

                outputArray.push(element);

                if (debug) {
                    console.log(outputArray);
                }

                // Create one long string from all the pieces, inserting
                // HTML line breaks between each
                outputString = outputArray.join('<br>');

            } else {
                outputString = inputString;
            }

            if (debug) {
                console.log('outputString: ' + outputString);
            }

            return outputString;
        },


        // Display text
        drawText : function (itemTitle, itemValue, fontColor) {

            var debug = false, data, layout, options, mainDataPlot, string1,
                string2;

            DATA_DISPLAY.calculatePlotSize();
            DATA_DISPLAY.showPlotCanvas();

            DATA_DISPLAY.enableImagePlotControls(false, false, false);

            DATA_DISPLAY.displayType = 'text';

            // Clean up the strings, and if they are long strings, they ought
            // to be split up
            string1 = DATA_DISPLAY.cleanupStringForDisplay(itemTitle);
            string2 = DATA_DISPLAY.cleanupStringForDisplay(itemValue);

            if (debug) {
                console.log('itemTitle  -->  itemValue:');
                console.log(itemTitle + ' --> ' + itemValue);
                console.log('string1  -->  string2:');
                console.log(string1 + ' --> ' + string2);
            }

            // Check for color choice
            fontColor = (fontColor === false ? '#ad3a3a' : fontColor);

            // Setup the empty data
            mainDataPlot = {
                z: [],
                colorscale: DATA_DISPLAY.colorScale,
            };

            // All the data that is to be plotted
            data = [mainDataPlot];

            // The layout of the plotting canvas and axes.
            layout = {
                "title" : '',
                "titlefont" : {
                    "color" : (DATA_DISPLAY.useDarkTheme === true ?
                            "#DDDDCE" : "#000000"),
                },
                "showlegend" : false,
                "autosize" : false,
                "width" : DATA_DISPLAY.plotWidth,
                "height" : 300,
                "paper_bgcolor" : (DATA_DISPLAY.useDarkTheme === true ?
                        '#181817' : '#ffffff'),
                "plot_bgcolor" : (DATA_DISPLAY.useDarkTheme === true ?
                        '#181817' : '#ffffff'),

                xaxis: {
                    title: '',
                    showgrid: false,
                    zeroline: false,
                    showticklabels : false,
                    ticks : '',
                },

                yaxis: {
                    title: '',
                    showgrid: false,
                    zeroline: false,
                    showticklabels : false,
                    ticks : '',
                },

                // More annotation examples here:
                //  https://plot.ly/javascript/text-and-annotations/
                annotations: [
                    {
                        x: 0,
                        y: 0,
                        xref: 'x',
                        yref: 'y',
                        text: '<b>' + string1 + '</b>' + '<br>' + string2,
                        showarrow: false,
                        font: {
                            family: 'Courier New, monospace',
                            size: 16,
                            color: fontColor,
                        },
                        align: 'center',
                        bordercolor: fontColor,
                        borderwidth: 3,
                        borderpad: 4,
                        bgcolor: 'rgba(255, 255, 255, 0.9)',
                        opacity: 0.8
                    }
                ],
            };

            options = {
                staticPlot: true,
                showLink: false,
                displaylogo: false,
                modeBarButtonsToRemove: [
                    'sendDataToCloud', 'hoverCompareCartesian',
                    'hoverClosestCartesian', 'resetScale2d', 'hoverClosest3d',
                    'resetCameraLastSave3d', 'orbitRotation', 'zoomIn2d',
                    'zoomOut2d'],
                displayModeBar: false,
                showTips: false,
            };

            DATA_DISPLAY.purgePlotCanvas();
            Plotly.newPlot(DATA_DISPLAY.plotCanvasDiv, data, layout, options
                ).then(
                AJAX_SPINNER.doneLoadingData()
            );
            DATA_DISPLAY.plotExists = true;

        },


        displayErrorMessage : function (inputUrl) {
            DATA_DISPLAY.enableImagePlotControls(false, false, false);
            DATA_DISPLAY.drawText('I don\'t know how to handle this yet!',
                'Sorry for the inconvenience :(',
                '#ad3a74', '');
            console.log('inputUrl: ' + inputUrl);
        },


        drawLine : function (value, nodeTitle) {

            var debug = false, data, layout, options;

            // Create data object
            data = [
                {
                    "y" : value,
                    "mode" : 'lines',
                    "type" : 'scatter'
                }
            ];

            // And the layout
            layout = {
                "showlegend" : false,
                "title" : (DATA_DISPLAY.mobileDisplay === true ?
                        '' : nodeTitle),
                "titlefont" : {
                    "color" : (DATA_DISPLAY.useDarkTheme === true ?
                            "#DDDDCE" : "#000000"),
                },
                "autosize" : false,
                "width" : DATA_DISPLAY.plotWidth,
                "height" : DATA_DISPLAY.plotHeight,
                "hovermode" : 'closest',
                "bargap" : 0,
                "paper_bgcolor" : (DATA_DISPLAY.useDarkTheme === true ?
                        '#181817' : '#ffffff'),
                "plot_bgcolor" : (DATA_DISPLAY.useDarkTheme === true ?
                        '#181817' : '#ffffff'),
                "margin" : {
                    l: 65,
                    r: 50,
                    b: 65,
                    t: 90,
                },
                "xaxis" : {
                    title: 'array index',
                    "titlefont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#DDDDCE" : "#000000"),
                    },
                    "tickfont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#999" : "#000000"),
                    },
                },
                "yaxis" : {
                    title: 'values',
                    "titlefont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#DDDDCE" : "#000000"),
                    },
                    "tickfont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#999" : "#000000"),
                    },
                }
            };

            options = {
                staticPlot: false,
                showLink: false,
                displaylogo: false,
                modeBarButtonsToRemove: [
                    'sendDataToCloud', 'hoverCompareCartesian',
                    'hoverClosestCartesian', 'resetScale2d', 'hoverClosest3d',
                    'resetCameraLastSave3d', 'orbitRotation', 'zoomIn2d',
                    'zoomOut2d'],
                // displayModeBar: true,
                displayModeBar: false,
                showTips: false,
                scrollZoom: true,
            };
            // All options are here:
            //  https://github.com/plotly/plotly.js/blob/master/src/plot_api/
            //      plot_config.js
            //  https://github.com/plotly/plotly.js/blob/master/src/components/
            //      modebar/buttons.js

            // Present them
            DATA_DISPLAY.purgePlotCanvas();
            Plotly.newPlot(DATA_DISPLAY.plotCanvasDiv, data, layout,
                options).then(AJAX_SPINNER.doneLoadingData()
                );
            DATA_DISPLAY.plotExists = true;

            DATA_DISPLAY.plotCanvasDiv.on('plotly_relayout',
                function (eventdata) {

                    if (debug) {
                        console.log('DATA_DISPLAY.plotCanvasDiv' +
                            'plotly_relayout line');
                    }

                    DATA_DISPLAY.handle1DZoom(eventdata);
                });
        },


        draw3DPlot : function () {

            var debug = false, data, layout, options, plotMargins, profiles,
                imageTitle;

            // Get the proper x & y axes ranges if this image has been
            // downsampled
            // profiles = DATA_DISPLAY.fillProfileHistograms(
            //     [0, DATA_DISPLAY.imageShapeDims[0],
            //         0, DATA_DISPLAY.imageShapeDims[1]]
            // );
            profiles = DATA_DISPLAY.fillProfileHistograms(
                DATA_DISPLAY.imageZoomSection
            );

            if (debug) {
                console.log('profiles.zMax: ' + profiles.zMax);
                console.log('profiles.zMin: ' + profiles.zMin);
            }

            // Create data object
            data = [
                {
                    z: DATA_DISPLAY.dataValues,
                    type: DATA_DISPLAY.plotType,
                    zsmooth: true,  // this might make plot interaction faster
                                    // if set true - not sure though
                    zauto : true,
                    zmin: profiles.zMin,
                    zmax: profiles.zMax,
                    cmin: profiles.zMin,
                    cmax: profiles.zMax,
                    x: profiles.imageXAxis,
                    y: profiles.imageYAxis,

                    // For mouse-over hover information, show the data values
                    // in the text field so that the same values appear when
                    // displaying the log values
                    hoverinfo: 'x+y+text',
                    text: DATA_DISPLAY.stringDataValues,

                    colorscale: DATA_DISPLAY.colorScale,
                    showscale : !DATA_DISPLAY.mobileDisplay,
                    colorbar : {
                        "title" : (DATA_DISPLAY.plotLogValues ? "10^" : ""),
                        "titleside" : "bottom",
                        "exponentformat" : "power",
                        "titlefont" : {
                            "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                    "#DDDDCE" : "#000000"),
                        },
                        "tickfont" : {
                            "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                    "#999" : "#000000"),
                        },
                    },
                }
            ];

            plotMargins =  { l: 65, r: 50, b: 65, t: 90, };
            if (DATA_DISPLAY.mobileDisplay) {
                plotMargins =  { l: 30, r: 20, b: 30, t: 20, };
            }

            imageTitle = DATA_DISPLAY.imageTitle;
            if (DATA_DISPLAY.imageSeries) {
                imageTitle += '-' +
                    DATA_DISPLAY.imageSeriesIndex;
            }

            // And the layout
            layout = {
                "showlegend" : false,
                "title" : (DATA_DISPLAY.mobileDisplay === true ?
                        '' : imageTitle),
                "titlefont" : {
                    "color" : (DATA_DISPLAY.useDarkTheme === true ?
                            "#999" : "#000000"),
                },
                "autosize" : false,
                "width" : DATA_DISPLAY.plotWidth,
                "height" : DATA_DISPLAY.plotHeight,
                "hovermode" : 'closest',
                "bargap" : 0,
                "paper_bgcolor" : (DATA_DISPLAY.useDarkTheme === true ?
                        '#181817' : '#ffffff'),
                "plot_bgcolor" : (DATA_DISPLAY.useDarkTheme === true ?
                        '#181817' : '#ffffff'),
                "margin" : plotMargins,
                "scene" : {
                    "xaxis" : {
                        "title" : "x",
                        "titlefont" : {
                            "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                    "#DDDDCE" : "#000000"),
                        },
                        "tickfont" : {
                            "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                    "#999" : "#000000"),
                        },
                    },
                    "yaxis" : {
                        "title" : "y",
                        "titlefont" : {
                            "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                    "#DDDDCE" : "#000000"),
                        },
                        "tickfont" : {
                            "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                    "#999" : "#000000"),
                        },
                    },
                    "zaxis" : {
                        "title" : "z",
                        "titlefont" : {
                            "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                    "#DDDDCE" : "#000000"),
                        },
                        "tickfont" : {
                            "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                    "#999" : "#000000"),
                        },
                        "type" : "linear",
                        "autorange" : true
                    }
                }
            };

            options = {
                staticPlot: false,
                showLink: false,
                displaylogo: false,
                modeBarButtonsToRemove: [
                    'sendDataToCloud', 'hoverCompareCartesian',
                    'hoverClosestCartesian', 'resetScale2d', 'hoverClosest3d',
                    'resetCameraLastSave3d', 'orbitRotation', 'zoomIn2d',
                    'zoomOut2d'],
                // displayModeBar: true,
                displayModeBar: false,
                showTips: false,
                scrollZoom: true,
            };
            // All options are here:
            //  https://github.com/plotly/plotly.js/blob/master/src/plot_api/
            //      plot_config.js
            //  https://github.com/plotly/plotly.js/blob/master/src/components/
            //      modebar/buttons.js

            // Present them
            DATA_DISPLAY.purgePlotCanvas();
            Plotly.newPlot(DATA_DISPLAY.plotCanvasDiv, data, layout,
                options).then(
                AJAX_SPINNER.doneLoadingData()
            );
            DATA_DISPLAY.plotExists = true;

            // Refill the profile histograms when a zoom event occurs
            // Why isn't this properly done already in the plotly library?!
            DATA_DISPLAY.plotCanvasDiv.on('plotly_relayout',
                function (eventdata) {

                    if (debug) {
                        console.log('DATA_DISPLAY.plotCanvasDiv' +
                            'plotly_relayout ' + DATA_DISPLAY.plotType);
                    }

                    DATA_DISPLAY.handle3DZoom(eventdata);
                });
        },


        // Fill x and y profile histograms, given the image and the dimensions
        // of the section of the image being viewed
        fillProfileHistograms : function (ranges) {

            var debug = false, i, j,
                imageYAxis = [], imageXAxis = [],
                yProfileXAxis = [], yProfileYValues = [],
                xProfileXAxis = [], xProfileYValues = [],
                xFactor = 1, yFactor = 1,
                zMin = 1e100, zMax = -1e100, xMin = ranges[0],
                xMax = ranges[1], yMin = ranges[2], yMax = ranges[3],
                xCoordinate, yCoordinate;

            // Check if this is a downsampled image - if so, change the axes
            // ranges
            if (DATA_DISPLAY.imageIsDownsampled) {
                xFactor = (xMax - xMin) / DATA_DISPLAY.dataValues[0].length;
                yFactor = (yMax - yMin) / DATA_DISPLAY.dataValues.length;
            }

            if (debug) {
                console.log('xFactor: ' + xFactor);
                console.log('yFactor: ' + yFactor);
                console.log('xMin:           ' + xMin);
                console.log('xMax:           ' + xMax);
                console.log('yMin:           ' + yMin);
                console.log('yMax:           ' + yMax);
                console.log('xFactor:        ' + xFactor);
                console.log('yFactor:        ' + yFactor);
                console.log('DATA_DISPLAY.dataValues.length: ' +
                    DATA_DISPLAY.dataValues.length);
                console.log('DATA_DISPLAY.dataValues[i].length: ' +
                    DATA_DISPLAY.dataValues[0].length);
                console.log('DATA_DISPLAY.loadedImageRange: ' +
                    DATA_DISPLAY.loadedImageRange);
                console.log('DATA_DISPLAY.loadedImageRangeSize: ' +
                    DATA_DISPLAY.loadedImageRangeSize);
                console.log('DATA_DISPLAY.imageZoomSection:   ' +
                    DATA_DISPLAY.imageZoomSection);
            }

            // Fill profile histograms
            for (i = 0; i < DATA_DISPLAY.dataValues.length; i += 1) {

                yCoordinate = DATA_DISPLAY.loadedImageRange[2] +
                    yFactor * i;

                // The y-profile values
                imageYAxis[i] = Math.round(yCoordinate);
                yProfileXAxis[i] = yCoordinate;
                yProfileYValues[i] = 0;

                for (j = 0; j < DATA_DISPLAY.dataValues[i].length; j += 1) {

                    xCoordinate = DATA_DISPLAY.loadedImageRange[0]
                        + j * xFactor;

                    // The x-profile values
                    if (i === 0) {
                        imageXAxis[j] = Math.round(xCoordinate);
                        xProfileXAxis[j] = xCoordinate;
                        xProfileYValues[j] = 0;
                    }

                    // Fill histograms for displayed image only
                    if (yCoordinate >= yMin && yCoordinate < yMax &&
                            xCoordinate >= xMin && xCoordinate < xMax) {

                        // Find the max and min values - used for setting
                        // colorscale range
                        if (DATA_DISPLAY.dataValues[i][j] < zMin) {
                            zMin = DATA_DISPLAY.dataValues[i][j];
                        }
                        if (DATA_DISPLAY.dataValues[i][j] > zMax) {
                            zMax = DATA_DISPLAY.dataValues[i][j];
                        }

                        // Fill the profile historgrams
                        yProfileYValues[i] +=
                            DATA_DISPLAY.dataValues[i][j];
                        xProfileYValues[j] +=
                            DATA_DISPLAY.dataValues[i][j];

                    }
                }
            }

            if (debug) {
                console.log('yProfileXAxis.length: ' + yProfileXAxis.length);
                console.log('yProfileYValues.length: ' +
                    yProfileYValues.length);
                console.log('xProfileXAxis.length: ' + xProfileXAxis.length);
                console.log('xProfileYValues.length: ' +
                    xProfileYValues.length);
                console.log('zMin: ' + zMin);
                console.log('zMax: ' + zMax);
                console.log('DATA_DISPLAY.plotLogValues: ' +
                    DATA_DISPLAY.plotLogValues);
            }

            return {
                imageYAxis : imageYAxis,
                imageXAxis : imageXAxis,
                yProfileXAxis : yProfileXAxis,
                yProfileYValues : yProfileYValues,
                xProfileXAxis : xProfileXAxis,
                xProfileYValues : xProfileYValues,
                zMin : zMin,
                zMax : zMax,
            };

        },


        // Check if an event is a zoom event
        isZoomEvent : function (eventdata) {

            var debug = false, i = 0, zoomEvent = false,
                rangeKeys = ['xaxis.range[0]', 'xaxis.range[1]',
                    'yaxis.range[0]', 'yaxis.range[1]'],
                autoKeys = ['xaxis.autorange', 'yaxis.autorange'];

            if (debug) {
                console.log(JSON.stringify(eventdata));
            }

            // Zoom events return json objects containing keys like
            // 'xaxis.range' or 'xaxis.autorange'
            for (i = 0; i < rangeKeys.length; i += 1) {
                if (eventdata.hasOwnProperty(rangeKeys[i])) {
                    zoomEvent = true;
                }
            }

            for (i = 0; i < autoKeys.length; i += 1) {
                if (eventdata.hasOwnProperty(autoKeys[i])) {
                    zoomEvent = true;
                }
            }

            return zoomEvent;

        },


        getZoomRange : function (eventdata) {

            var debug = false, i = 0, ranges = [-1, -1, -1, -1],
                plotLayout, rangeKeys = ['xaxis.range[0]', 'xaxis.range[1]',
                'yaxis.range[0]', 'yaxis.range[1]'],
                autoKeys = ['xaxis.autorange', 'yaxis.autorange'];

            if (debug) {
                // Get the present layout range
                plotLayout = DATA_DISPLAY.plotCanvasDiv.layout;
                console.log('plotLayout: ');
                console.log(plotLayout);
            }

            // Loop over the 4 range values - x & y, min & max
            for (i = 0; i < ranges.length; i += 1) {

                // Look at the 'range' keys, set ranges
                if (eventdata.hasOwnProperty(rangeKeys[i])) {

                    ranges[i] = eventdata[rangeKeys[i]];

                    if (debug) {
                        console.log(rangeKeys[i]);
                        console.log(eventdata[rangeKeys[i]]);
                    }

                }

                // If any new min or max has not yet been set, then
                // use the present values
                if (ranges[i] === -1) {
                    ranges[i] = DATA_DISPLAY.loadedImageRange[i];
                }

                // Round to the nearest integer
                ranges[i] = Math.round(ranges[i]);

                // Need to add 1 to end of ranges
                if (!DATA_DISPLAY.isEven(i)) {
                    ranges[i] += 1;
                    if (debug) {
                        console.log('ranges[' + i + ']: ' + ranges[i]);
                    }
                }
            }

            // Make sure we haven't gone too far
            if (ranges[0] < 0) {
                ranges[0] = 0;
            }
            if (ranges[1] >= DATA_DISPLAY.imageShapeDims[0]) {
                ranges[1] = DATA_DISPLAY.imageShapeDims[0];
            }
            if (ranges[2] < 0) {
                ranges[2] = 0;
            }
            if (ranges[3] >= DATA_DISPLAY.imageShapeDims[1]) {
                ranges[3] = DATA_DISPLAY.imageShapeDims[1];
            }


            // Check for reset-zoom events
            if (eventdata.hasOwnProperty(autoKeys[0])) {
                ranges[0] = 0;
                ranges[1] = DATA_DISPLAY.imageShapeDims[0];
            }
            if (eventdata.hasOwnProperty(autoKeys[1])) {
                ranges[2] = 0;
                ranges[3] = DATA_DISPLAY.imageShapeDims[1];
            }

            if (debug) {
                console.log('x-axis start: ' + ranges[0]);
                console.log('x-axis end:   ' + ranges[1]);
                console.log('y-axis start: ' + ranges[2]);
                console.log('y-axis end:   ' + ranges[3]);
            }

            // Save for later
            DATA_DISPLAY.imageZoomSection = ranges;

            return ranges;
        },


        // Do what it takes to handle a zoom event in a 3D image
        handle3DZoom : function (eventdata) {

            var debug = false, promises = [], newImageFetched = false,
                resetZoomEvent = false;

            if (debug) {
                console.log(JSON.stringify(eventdata));
            }

            // Try and guess if the 'Rest camera to default' button has been
            // pressed - there must be a better way!
            // If so, get a brand spanking new image, which is necessary if
            // we are already zoomed in on a previously downsampled image.
            if (eventdata.hasOwnProperty('scene')) {
                if (eventdata.scene.hasOwnProperty('eye')) {
                    if (eventdata.scene.eye.x === 1.25 &&
                            eventdata.scene.eye.y === 1.25 &&
                            eventdata.scene.eye.z === 1.25) {

                        if (debug) {
                            console.log('reset button pressed?');
                        }
                        resetZoomEvent = true;

                    }
                }
            }

            if (resetZoomEvent) {

                // For an image series
                if (DATA_DISPLAY.imageSeries) {
                    promises.push(
                        HANDLE_DATASET.imageSeriesInput(
                            DATA_DISPLAY.imageSeriesIndex,
                            false,
                            false,
                            true
                        )
                    );

                // For an image
                } else {
                    promises.push(
                        HANDLE_DATASET.displayImage(
                            DATA_DISPLAY.imageTargetUrl,
                            false,
                            false,
                            false
                        )
                    );
                }

                newImageFetched = true;

                if (debug) {
                    console.log('newImageFetched: ' +
                        newImageFetched);
                }

                if (newImageFetched) {

                    $.when.apply(null, promises).done(
                        function () {
                            DATA_DISPLAY.updatePlotZData(false,
                                newImageFetched,
                                true);
                        }
                    );
                }

                $('#plotControlReset').hide();
            } else {
                $('#plotControlReset').show();
            }

        },

        // Do what it takes to handle a zoom event in a 2D image
        handle1DZoom : function (eventdata) {

            var debug = false;

            // Check if this is a zoom event
            if (!DATA_DISPLAY.isZoomEvent(eventdata)) {
                if (debug) {
                    console.log('** DATA_DISPLAY.handle1DZoom Does not Look' +
                        ' like a zoom event, exiting');
                }
                return;
            }

            if (debug) {
                console.log('** DATA_DISPLAY.handle1DZoom Plot zoom event **');
                console.log(eventdata);
            }

            $('#plotControlReset').show();
        },

        // Do what it takes to handle a zoom event in a 2D image
        handle2DZoom : function (eventdata) {

            var debug = false, i = 0, ranges = [-1, -1, -1, -1], section,
                promises = [], newImageFetched = false, resetZoomEvent = false,
                autoKeys = ['xaxis.autorange', 'yaxis.autorange'];

            if (debug) {
                console.log('** START DATA_DISPLAY.handle2DZoom Plot ' +
                    'zoom event **');
            }

            // Check if this is a zoom event
            if (!DATA_DISPLAY.isZoomEvent(eventdata)) {
                if (debug) {
                    console.log('** DATA_DISPLAY.handle2DZoom Does not Look' +
                        ' like a zoom event, exiting');
                }
                return;
            }

            // Get the zoom range
            ranges = DATA_DISPLAY.getZoomRange(eventdata);

            if (debug) {
                console.log('  x-axis start: ' + ranges[0]);
                console.log('  x-axis end:   ' + ranges[1]);
                console.log('  y-axis start: ' + ranges[2]);
                console.log('  y-axis end:   ' + ranges[3]);
            }

            // Check for reset-zoom events
            for (i = 0; i < autoKeys.length; i += 1) {
                if (eventdata.hasOwnProperty(autoKeys[i])) {
                    resetZoomEvent = true;
                }
            }

            // Show the 'Reset Zoom' button
            if (resetZoomEvent === false) {
                $('#plotControlReset').show();
            }

            if (debug) {
                console.log('DATA_DISPLAY.imageSeries: ' +
                    DATA_DISPLAY.imageSeries);
                console.log('DATA_DISPLAY.imageIsDownsampled: ' +
                    DATA_DISPLAY.imageIsDownsampled);
                console.log('DATA_DISPLAY.usingOriginalImage: ' +
                    DATA_DISPLAY.usingOriginalImage);
                console.log('DATA_DISPLAY resetZoomEvent: ' + resetZoomEvent);
            }

            // If the original image was downsampled, fetch a new image from
            // the server
            if (DATA_DISPLAY.imageIsDownsampled || resetZoomEvent) {

                // Start the loading thingy
                AJAX_SPINNER.startLoadingData(10);

                // For an image series
                if (DATA_DISPLAY.imageSeries) {
                    section = [DATA_DISPLAY.imageSeriesIndex,
                              DATA_DISPLAY.imageSeriesIndex + 1,
                              ranges[0], ranges[1], ranges[2], ranges[3]];
                } else {
                    section = ranges;
                }

                promises.push(
                    HANDLE_DATASET.displayImage(
                        DATA_DISPLAY.imageTargetUrl,
                        section,
                        // false,
                        true,
                        false
                    )
                );

                newImageFetched = true;
            }


            if (debug) {
                console.log('newImageFetched: ' + newImageFetched);
            }

            if (newImageFetched) {
                DATA_DISPLAY.loadedImageRange = ranges;
            }

            // After a new image has been fetched (or if there was no need to
            // fetch an image), refill the plot and shit
            $.when.apply(null, promises).done(
                function () {
                    // DATA_DISPLAY.updatePlotZData(ranges, newImageFetched,
                    //     true);
                    if (debug) {
                        console.log('done');
                    }
                }
            );

            if (debug) {
                console.log('** END DATA_DISPLAY.handle2DZoom Plot zoom ' +
                    'event **');
            }
        },


        // Plot the image as a 2D heatmap along with x and y profile histograms
        // that update when zooming
        draw2DPlot : function () {

            var debug = false, profiles, xProfilePlot, yProfilePlot, data,
                layout, options, mainDataPlot, plotMargins = {}, imageTitle;

            if (debug) {
                console.log('DATA_DISPLAY.imageShapeDims[0]' +
                    DATA_DISPLAY.imageShapeDims[0]);
                console.log('DATA_DISPLAY.imageShapeDims[1]' +
                    DATA_DISPLAY.imageShapeDims[1]);
            }

            // Create profile histograms and calculate the range of the axes
            profiles = DATA_DISPLAY.fillProfileHistograms(
                DATA_DISPLAY.imageZoomSection
            );

            // The primary, 2-dimensional plot of the data - works best as a
            // 'heatmap' plot me thinks
            mainDataPlot = {
                z: DATA_DISPLAY.dataValues,
                type: DATA_DISPLAY.plotType,
                zsmooth: false,
                zmin: profiles.zMin,
                zmax: profiles.zMax,
                x: profiles.imageXAxis,
                y: profiles.imageYAxis,

                // For mouse-over hover information, show the data values in
                // the text field so that the same values appear when
                // displaying the log values
                hoverinfo: 'x+y+text',
                text: DATA_DISPLAY.stringDataValues,

                colorscale: DATA_DISPLAY.colorScale,
                showscale : !DATA_DISPLAY.mobileDisplay,
                colorbar : {
                    "title" : (DATA_DISPLAY.plotLogValues ? "10^" : ""),
                    "titleside" : "bottom",
                    "exponentformat" : "power",
                    "titlefont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#DDDDCE" : "#000000"),
                    },
                    "tickfont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#999" : "#000000"),
                    },
                },
            };

            if (debug) {
                console.log('DATA_DISPLAY.colorProfile: ' +
                    DATA_DISPLAY.colorProfile);
            }

            // The x-profile of the plot, displayed as a bar chart / histogram
            xProfilePlot = {
                x: profiles.xProfileXAxis,
                y: profiles.xProfileYValues,
                xmax: 500,
                ymax: 500,
                zmax: 500,
                name: 'x profile',
                marker: {color: DATA_DISPLAY.colorProfile},
                yaxis: 'y2',
                type: 'bar'
            };

            // The y-profile, also displayed as a bar chart / histogram,
            // oriented horizontally --> switch x & y
            yProfilePlot = {
                x: profiles.yProfileYValues,
                y: profiles.yProfileXAxis,
                name: 'y profile',
                marker: {color: DATA_DISPLAY.colorProfile},
                xaxis: 'x2',
                type: 'bar',
                orientation: 'h'
            };

            // All the data that is to be plotted
            data = [mainDataPlot, xProfilePlot, yProfilePlot];

            // Padding around the plotting canvas - less for mobile devices
            plotMargins =  { l: 65, r: 50, b: 65, t: 90, };
            if (DATA_DISPLAY.mobileDisplay) {
                plotMargins =  { l: 30, r: 20, b: 30, t: 20, };
            }

            imageTitle = DATA_DISPLAY.imageTitle;
            if (DATA_DISPLAY.imageSeries) {
                imageTitle += '-' +
                    DATA_DISPLAY.imageSeriesIndex;
            }

            // The layout of the plotting canvas and axes. Note that the amount
            // of space each plot takes up is a range from 0 to 1, and follows
            // the keyword 'domain'
            layout = {
                "title" : (DATA_DISPLAY.mobileDisplay === true ?
                        '' : imageTitle),
                "titlefont" : {
                    "color" : (DATA_DISPLAY.useDarkTheme === true ?
                            "#DDDDCE" : "#000000"),
                },
                "showlegend" : false,
                "autosize" : false,

                "width": DATA_DISPLAY.plotWidth,
                "height": DATA_DISPLAY.plotHeight,
                "margin": plotMargins,

                "hovermode" : 'closest',
                "bargap" : 0,
                "paper_bgcolor" : (DATA_DISPLAY.useDarkTheme === true ?
                        '#181817' : '#ffffff'),
                "plot_bgcolor" : (DATA_DISPLAY.useDarkTheme === true ?
                        '#181817' : '#ffffff'),

                "xaxis" : {
                    "title" : "x",
                    "domain" : [0, 0.85],
                    "showgrid" : false,
                    "zeroline" : false,
                    "titlefont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#DDDDCE" : "#000000"),
                    },
                    "tickfont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#999" : "#000000"),
                    },

                    // Settings for square pixels
                    "constraintoward" : "center",
                },

                "yaxis" : {
                    "title" : "y",
                    "domain" : [0, 0.85],
                    "showgrid" : false,
                    "zeroline": false,
                    "titlefont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#DDDDCE" : "#000000"),
                    },
                    "tickfont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#999" : "#000000"),
                    },

                    // Settings for square pixels
                    "scaleanchor" : (DATA_DISPLAY.plotAspect === true ?
                                "x" : ""),
                    "scaleratio" : 1,
                    "constraintoward" : "middle",
                },

                "xaxis2" : {
                    "domain" : [0.85, 1],
                    "showgrid" : false,
                    "zeroline" : false,
                    "titlefont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#DDDDCE" : "#000000"),
                    },
                    "tickfont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#999" : "#000000"),
                    },
                },

                "yaxis2" : {
                    "domain" : [0.85, 1],
                    "showgrid" : false,
                    "zeroline" : false,
                    "titlefont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#DDDDCE" : "#000000"),
                    },
                    "tickfont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#999" : "#000000"),
                    },
                },
            };

            if (debug) {
                console.log('layout:');
                console.log(layout);
            }

            options = {
                staticPlot: false,
                showLink: false,
                displaylogo: false,
                modeBarButtonsToRemove: [
                    'sendDataToCloud', 'hoverCompareCartesian',
                    'hoverClosestCartesian', 'resetScale2d', 'hoverClosest3d',
                    'resetCameraLastSave3d', 'orbitRotation', 'zoomIn2d',
                    'zoomOut2d'],
                displayModeBar: false,
                showTips: false,
                scrollZoom: true,
            };
            // All options are here:
            //  https://github.com/plotly/plotly.js/blob/master/src/plot_api/
            //      plot_config.js
            //  https://github.com/plotly/plotly.js/blob/master/src/components/
            //      modebar/buttons.js

            DATA_DISPLAY.purgePlotCanvas();
            Plotly.newPlot(DATA_DISPLAY.plotCanvasDiv, data, layout,
                options).then(
                AJAX_SPINNER.doneLoadingData()
            );
            DATA_DISPLAY.plotExists = true;

            // Refill the profile histograms when a zoom event occurs
            // Why isn't this properly done already in the plotly library?!
            DATA_DISPLAY.plotCanvasDiv.on('plotly_relayout',
                function (eventdata) {

                    if (debug) {
                        console.log('DATA_DISPLAY.plotCanvasDiv' +
                            'plotly_relayout ' + DATA_DISPLAY.plotType);
                    }

                    DATA_DISPLAY.handle2DZoom(eventdata);
                });

        },


        redrawPlotCanvas : function (timeDelay) {

            var debug = false, plotHeight = DATA_DISPLAY.plotHeight;

            if (DATA_DISPLAY.plotExists) {

                // During a window resize event, the resize function will be
                // called several times per second, on the order of 15 Hz! Best
                // to wait a bit try to just resize once, as it's a bit costly
                // for plotly to execute relayout
                clearTimeout(DATA_DISPLAY.resizeTimer);

                AJAX_SPINNER.startLoadingData(50);

                DATA_DISPLAY.resizeTimer = setTimeout(function () {

                    if (debug) {
                        console.log('about to run Plotly.relayout');
                    }

                    // Calculate the plot dimensions and save them
                    DATA_DISPLAY.calculatePlotSize();

                    // Use smaller canvas when displaying text instead of
                    // images
                    if (DATA_DISPLAY.displayType === 'text') {
                        plotHeight = 300;
                    } else {
                        plotHeight = DATA_DISPLAY.plotHeight;
                    }

                    Plotly.relayout(DATA_DISPLAY.plotCanvasDiv, {
                        width: DATA_DISPLAY.plotWidth,
                        height: plotHeight,
                    }).then(
                        AJAX_SPINNER.doneLoadingData()
                    );

                }, timeDelay);
            }

        },


        // Calculate the plot size - needs to be improved for small screens
        calculatePlotSize : function () {

            var debug = false, newPlotDivHeight, newPlotDivWidth,
                windowWidth = $(window).width(),
                windowHeight = $(window).height(),
                containerWidth = $('#displayContainer').width(),
                containerHeight = $('#displayContainer').height(),
                divWidth = $('#plotCanvasDiv').width(),
                divHeight = $('#plotCanvasDiv').height();

            newPlotDivHeight = windowHeight - 80;
            if (DATA_DISPLAY.imageSeries) {
                newPlotDivHeight -= 33;
            }

            // For smaller screens, fuck padding
            // if (windowWidth > 978) {
            //     DATA_DISPLAY.mobileDisplay = false;
            // } else {
            //     DATA_DISPLAY.mobileDisplay = true;
            // }
            DATA_DISPLAY.mobileDisplay = window.mobilecheck();

            newPlotDivWidth = containerWidth;
            if (!DATA_DISPLAY.mobileDisplay) {
                newPlotDivWidth -= 40;
            }

            if (debug) {
                console.log('DATA_DISPLAY.imageSeries: ' +
                    DATA_DISPLAY.imageSeries);
                console.log('DATA_DISPLAY.mobileDisplay: ' +
                    DATA_DISPLAY.mobileDisplay);
                console.log('windowWidth:  ' + windowWidth);
                console.log('windowHeight: ' + windowHeight);
                console.log('divWidth:     ' + divWidth);
                console.log('divHeight:    ' + divHeight);
                console.log('containerWidth:   ' + containerWidth);
                console.log('containerHeight:  ' + containerHeight);
                console.log('newPlotDivHeight: ' + newPlotDivHeight);
                console.log('newPlotDivWidth:  ' + newPlotDivWidth);
            }

            $('#plotCanvasDiv').height(newPlotDivHeight);
            DATA_DISPLAY.plotWidth = newPlotDivWidth;
            DATA_DISPLAY.plotHeight = newPlotDivHeight;
        },


        // Plot the data!
        plotLine : function (value, imageTitle) {

            DATA_DISPLAY.displayType = 'line';

            DATA_DISPLAY.showPlotCanvas();

            DATA_DISPLAY.calculatePlotSize();

            DATA_DISPLAY.lineValues = value;
            DATA_DISPLAY.lineTitle = imageTitle;

            DATA_DISPLAY.drawLine(DATA_DISPLAY.lineValues,
                DATA_DISPLAY.lineTitle);

        },


        // Plot the data! This redraws everything
        plotData : function () {

            DATA_DISPLAY.displayType = 'image';

            DATA_DISPLAY.showPlotCanvas();

            DATA_DISPLAY.calculatePlotSize();

            if (DATA_DISPLAY.plotDimension === 2) {
                DATA_DISPLAY.draw2DPlot();
            } else {
                DATA_DISPLAY.draw3DPlot();
            }

        },


        // Change the data used in the plot without redrawing everything
        updatePlotZData : function (ranges, newImageFetched, setAxesRange) {

            var debug = false, profiles;

            if (debug) {
                console.log('** updatePlotZData **');
                console.log('refilling histograms');
                console.log('ranges:');
                console.log(ranges);
                console.log('newImageFetched: ' + newImageFetched);
            }

            if (!ranges) {
                if (DATA_DISPLAY.imageZoomSection) {
                    ranges = DATA_DISPLAY.imageZoomSection;
                } else {
                    ranges = DATA_DISPLAY.loadedImageRange;
                }
            }

            // Refill the profile histograms
            profiles = DATA_DISPLAY.fillProfileHistograms(ranges);

            if (debug) {
                console.log('profiles:');
                console.log(profiles);
            }

            if (newImageFetched) {

                // Refill the 2D or 3D plot, set the min and max so the color
                // bar range updates
                Plotly.restyle(DATA_DISPLAY.plotCanvasDiv, {
                    z: [DATA_DISPLAY.dataValues],
                    text: [DATA_DISPLAY.stringDataValues],
                    x: [profiles.imageXAxis],
                    y: [profiles.imageYAxis],
                    zmin: [profiles.zMin],
                    zmax: [profiles.zMax],
                    colorscale: [DATA_DISPLAY.colorScale],
                    colorbar : [{
                        "title" : (DATA_DISPLAY.plotLogValues ? "10^" : ""),
                        "titleside" : "bottom",
                        "exponentformat" : "power",
                        "titlefont" : {
                            "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                    "#DDDDCE" : "#000000"),
                        },
                        "tickfont" : {
                            "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                    "#999" : "#000000"),
                        },
                    }],
                }, [0]);

                // Set the ranges of the 2D plot properly - otherwise empty
                // bands will appears when not centered on data.
                if (DATA_DISPLAY.plotDimension === 2) {
                    if (debug) {
                        console.log('DATA_DISPLAY.imageZoomSection:');
                        console.log(DATA_DISPLAY.imageZoomSection[0]);
                        console.log(DATA_DISPLAY.imageZoomSection[1]);
                        console.log(DATA_DISPLAY.imageZoomSection[2]);
                        console.log(DATA_DISPLAY.imageZoomSection[3]);
                        console.log('DATA_DISPLAY.plotAspect: ' +
                            DATA_DISPLAY.plotAspect);
                    }

                    if (setAxesRange) {
                        console.log('setting axes ranges - doing another' +
                            ' relayout');

                        // Also, the domain needs to be set again, not sure
                        // why...
                        Plotly.relayout(DATA_DISPLAY.plotCanvasDiv, {
                            "xaxis" : {
                                // "range" : [
                                //     DATA_DISPLAY.imageZoomSection[0] - 0.5,
                                //     DATA_DISPLAY.imageZoomSection[1] - 0.5],
                                "autorange" : true,
                                "domain" : [0, 0.85]
                            },

                            "yaxis": {
                                // "range" : [
                                //     DATA_DISPLAY.imageZoomSection[2] - 0.5,
                                //     DATA_DISPLAY.imageZoomSection[3] - 0.5],
                                "autorange" : true,
                                "domain" : [0, 0.85],

                                "scaleanchor" : ["x"],
                                "scaleratio" : [1],
                                "constraintoward" : ["top"],
                            },
                        });
                    }
                }

                // If an image series, change the title
                if (DATA_DISPLAY.imageSeries) {
                    Plotly.relayout(DATA_DISPLAY.plotCanvasDiv, {
                        "title" : (DATA_DISPLAY.mobileDisplay === true ?
                                '' : DATA_DISPLAY.imageTitle + '-' +
                                DATA_DISPLAY.imageSeriesIndex),
                    });
                }

            } else {
                // Set the min and max so the color bar range updates
                Plotly.restyle(DATA_DISPLAY.plotCanvasDiv, {
                    zmin: [profiles.zMin],
                    zmax: [profiles.zMax],
                }, [0]);
            }

            if (DATA_DISPLAY.plotDimension === 2) {
                // Update the profile histograms in the plot
                Plotly.restyle(DATA_DISPLAY.plotCanvasDiv, {
                    x: [profiles.xProfileXAxis],
                    y: [profiles.xProfileYValues],
                    marker: [{color: DATA_DISPLAY.colorProfile}],
                }, [1]);

                Plotly.restyle(DATA_DISPLAY.plotCanvasDiv, {
                    x: [profiles.yProfileYValues],
                    y: [profiles.yProfileXAxis],
                    marker: [{color: DATA_DISPLAY.colorProfile}],
                }, [2]);
            }

            AJAX_SPINNER.doneLoadingData();

            if (debug) {
                console.log('** End of updatePlotZData **');
            }
        },


        // Change the plot type - 2D 'heatmap' or 3D 'surface' seem to be the
        // best options
        changeType : function (type) {

            if (type !== '') {

                // Toggle which item in the list in highlighted
                $("#plotType" +
                    DATA_DISPLAY.plotType).removeClass('selected');
                $("#plotType" + type).addClass('selected');
                $("#plotType" +
                    DATA_DISPLAY.plotType + "Mobile").removeClass('selected');
                $("#plotType" + type + "Mobile").addClass('selected');


                // Save the new plot type
                DATA_DISPLAY.plotType = type;

                if (DATA_DISPLAY.plotType === 'heatmap') {
                    DATA_DISPLAY.plotDimension = 2;
                } else {
                    DATA_DISPLAY.plotDimension = 3;
                }

                DATA_DISPLAY.purgePlotCanvas();
                AJAX_SPINNER.startLoadingData(1);

                // Use a bit of a delay just so that the loading spinner has
                // a chance to start up
                setTimeout(function () {
                    DATA_DISPLAY.plotData();
                }, 300);
            }

        },


        // Switch between the use of log and non-log values
        toggleAspect : function () {

            var debug = false;

            if (DATA_DISPLAY.plotAspect) {
                DATA_DISPLAY.plotAspect = false;
                $("#aspectTooltip").html('Aspect ratio is not kept');
                $('#aspectIcon').hide();
                $('#noAspectIcon').show();
            } else {
                DATA_DISPLAY.plotAspect = true;
                $("#aspectTooltip").html('Aspect ratio is kept');
                $('#aspectIcon').show();
                $('#noAspectIcon').hide();
            }

            if (debug) {
                console.log('DATA_DISPLAY.plotAspect: ' +
                    DATA_DISPLAY.plotAspect);
            }

            DATA_DISPLAY.purgePlotCanvas();
            AJAX_SPINNER.startLoadingData(1);

            // Use a bit of a delay just so that the loading spinner has
            // a chance to start up
            setTimeout(function () {
                DATA_DISPLAY.plotData();
            }, 300);
        },


        // Switch between the use of log and non-log values
        toggleType : function () {

            var debug = false;

            if (DATA_DISPLAY.plotDimension === 2) {

                DATA_DISPLAY.plotDimension = 3;
                DATA_DISPLAY.plotType = 'surface';
                $("#dimensionTooltip").html('3D image displayed');
                $('#2DIcon').hide();
                $('#3DIcon').show();
                $('#plotControlAspect').hide();

            } else {

                DATA_DISPLAY.plotDimension = 2;
                DATA_DISPLAY.plotType = 'heatmap';
                $("#dimensionTooltip").html('2D image displayed');
                $('#3DIcon').hide();
                $('#2DIcon').show();
                $('#plotControlAspect').show();

            }

            if (debug) {
                console.log('DATA_DISPLAY.plotDimension: ' +
                    DATA_DISPLAY.plotDimension);
                console.log('DATA_DISPLAY.plotType: ' + DATA_DISPLAY.plotType);
            }

            DATA_DISPLAY.purgePlotCanvas();
            AJAX_SPINNER.startLoadingData(1);

            // Use a bit of a delay just so that the loading spinner has
            // a chance to start up
            setTimeout(function () {
                DATA_DISPLAY.plotData();
            }, 300);
        },


        // Change the color map
        changeColor : function (colorScale) {

            var debug = false;

            if (debug) {
                console.log('colorScale: ' + colorScale);
            }

            if (colorScale !== '') {

                // Toggle which item in the menu is highlighted
                $('#' + DATA_DISPLAY.colorScale + 'Icon').hide();
                $("#plotColor" +
                    DATA_DISPLAY.colorScale).removeClass('selected');
                $("#plotColor" + DATA_DISPLAY.colorScale +
                    "Mobile").removeClass('selected');

                $("#plotColor" +
                    colorScale).addClass('selected');
                $("#plotColor" + colorScale +
                    "Mobile").addClass('selected');

                // Save new color choice
                DATA_DISPLAY.colorScale = colorScale;

                // Set the tooltip
                // $("#colormapTooltip").html('Colormap: ' + colorScale);

                // Set the 2D profile histogram color
                switch (colorScale) {
                case 'Electric':
                    DATA_DISPLAY.colorProfile = 'rgb(204,0,153)';
                    break;
                case 'Greys':
                    DATA_DISPLAY.colorProfile = 'rgb(115,115,115)';
                    break;
                case 'Hot':
                    DATA_DISPLAY.colorProfile = 'rgb(153,0,0)';
                    break;
                case 'Jet':
                    DATA_DISPLAY.colorProfile = 'rgb(0,153,255)';
                    break;
                case 'Picnic':
                    DATA_DISPLAY.colorProfile = 'rgb(255,153,221)';
                    break;
                default:
                    DATA_DISPLAY.colorProfile = 'rgb(102,0,0)';
                }

                // And the sweet little icon
                $('#' + colorScale + 'Icon').show();

                if (debug) {
                    console.log('DATA_DISPLAY.colorScale:');
                    console.log(DATA_DISPLAY.colorScale);
                    console.log('DATA_DISPLAY.colorProfile: ' +
                        DATA_DISPLAY.colorProfile);
                }

                // Change the plot color using a restyle event
                if (DATA_DISPLAY.plotExists) {
                    Plotly.restyle(DATA_DISPLAY.plotCanvasDiv, {
                        colorscale: DATA_DISPLAY.colorScale
                    }, [0]);

                    // Update the profile histograms in the plot
                    if (DATA_DISPLAY.plotDimension === 2) {
                        Plotly.restyle(DATA_DISPLAY.plotCanvasDiv, {
                            marker: [{color: DATA_DISPLAY.colorProfile}],
                        }, [1]);

                        Plotly.restyle(DATA_DISPLAY.plotCanvasDiv, {
                            marker: [{color: DATA_DISPLAY.colorProfile}],
                        }, [2]);
                    }
                }
            }
        },


        // One of the plotly buttons remade - reset zoom / autoscale
        resetZoom : function () {

            var debug = false;

            if (debug) {
                console.log();
            }

            // Start the spinner
            AJAX_SPINNER.startLoadingData(1);

            // The simplest way to reset the zoom seems to be to just replot
            // everything

            // For an image series
            if (DATA_DISPLAY.imageSeries) {

                HANDLE_DATASET.imageSeriesInput(DATA_DISPLAY.imageSeriesIndex,
                    false);

            // For an image
            } else if (DATA_DISPLAY.displayType === 'image') {

                HANDLE_DATASET.displayImage(
                    DATA_DISPLAY.imageTargetUrl,
                    false,
                    true,
                    false
                );

            // For a line
            } else if (DATA_DISPLAY.displayType === 'line') {

                DATA_DISPLAY.drawLine(DATA_DISPLAY.lineValues,
                    DATA_DISPLAY.lineTitle);

            }

            // Hide the 'Reset Zoom' button
            $('#plotControlReset').hide();
        },


        // One of the plotly buttons remade - download the plot image
        downloadPlot : function () {

            // Get the plot dimensions
            var debug = false, divWidth = $('#plotCanvasDiv').width(),
                divHeight = $('#plotCanvasDiv').height();

            // Start the spinner
            AJAX_SPINNER.startLoadingData(1);

            // Download that shit! Wait a bit so that the loader is visible :)
            setTimeout(function () {

                if (debug) {
                    console.log('starting plotly download function');
                }

                Plotly.downloadImage(
                    DATA_DISPLAY.plotCanvasDiv,
                    {
                        format: 'png',
                        width: divWidth,
                        height: divHeight,
                        filename: 'newplot'
                    }
                ).then(
                    // There seems to be a delay between when plotly is done
                    // creating 3D images, and when the download dialog appears
                    AJAX_SPINNER.doneLoadingData(1000)
                );
            }, 50);
        },


        // Switch between the use of automatic image manipulation or not
        toggleAutoImage : function (useAutoLimits) {

            var debug = false;

            if (debug) {
                console.log('useAutoLimits: ' + useAutoLimits);
            }

            if (useAutoLimits === undefined) {
                DATA_DISPLAY.plotAutoLimits = !DATA_DISPLAY.plotAutoLimits;
            } else {
                DATA_DISPLAY.plotAutoLimits = useAutoLimits;
            }

            if (debug) {
                console.log('DATA_DISPLAY.plotAutoLimits: ' +
                    DATA_DISPLAY.plotAutoLimits);
            }

            // Change the color of the button and the text displayed
            if (DATA_DISPLAY.plotAutoLimits) {
                if (debug) {
                    console.log('Automatic intensity limits in use');
                }
                $("#autoImageButton").html(
                    'Automatic intensity limits in use'
                );
                $('#autoLimitsIcon').show();
                $('#noAutoLimitsIcon').hide();
            } else {
                if (debug) {
                    console.log('Automatic Limits?');
                }
                $("#autoImageButton").html(
                    'Automatic intensity limits not in use'
                );
                $('#autoLimitsIcon').hide();
                $('#noAutoLimitsIcon').show();
            }

            // Set the data that will be used for plotting
            DATA_DISPLAY.setDataToDisplay();

            AJAX_SPINNER.startLoadingData(1);

            // Use a bit of a delay just so that the loading spinner has a
            // chance to start up and the plot image has time to change
            setTimeout(function () {
                DATA_DISPLAY.updatePlotZData(false, true, false);
            }, 300);
        },



        // Switch between the use of log and non-log values
        toggleLogPlot : function (useLog) {

            var debug = false;

            if (debug) {
                console.log('useLog: ' + useLog);
            }

            if (useLog === undefined) {
                DATA_DISPLAY.plotLogValues = !DATA_DISPLAY.plotLogValues;
            } else {
                DATA_DISPLAY.plotLogValues = useLog;
            }

            if (debug) {
                console.log('DATA_DISPLAY.plotLogValues: ' +
                    DATA_DISPLAY.plotLogValues);
            }

            // Change the data to be displayed
            if (DATA_DISPLAY.plotLogValues) {
                if (debug) {
                    console.log('Log Plot!');
                }

                $("#logPlotButton").html('Log of intensity displayed');
                $('#logIcon').show();
                $('#noLogIcon').hide();

            } else {
                if (debug) {
                    console.log('Log Plot?');
                }

                $("#logPlotButton").html('Log of intensity not displayed');
                $('#logIcon').hide();
                $('#noLogIcon').show();
            }

            // Set the data that will be used for plotting
            DATA_DISPLAY.setDataToDisplay();

            AJAX_SPINNER.startLoadingData(1);

            // Use a bit of a delay just so that the loading spinner has a
            // chance to start up and the log button background has time to
            // change color :p
            setTimeout(function () {
                DATA_DISPLAY.updatePlotZData(false, true, false);
            }, 300);
        },


        // Create several images using:
        //  - Raw data
        //  - Log of raw data
        //  - Raw data with bad pixels removed
        //  - Log of raw data with bad pixels removed
        createImageArrays : function (value, bpr_limits, badPixelsExist) {

            var debug = false, i, j;

            if (debug) {
                console.log('*** START data-display.js createImageArrays');
                console.log('bpr_limits: ');
                console.log(bpr_limits);
                console.log('badPixelsExist: ' + badPixelsExist);
            }

            // Array containing raw data
            DATA_DISPLAY.initialDataValues = value;

            // The log of the raw data
            DATA_DISPLAY.logOfDataValues = [];

            // The raw data in string form
            DATA_DISPLAY.stringDataValues = [];

            // Any bad pixels?
            DATA_DISPLAY.badPixelsExist = badPixelsExist;

            // Arrays containing data with bad pixels removed (BPR)
            if (badPixelsExist) {
                DATA_DISPLAY.initialDataValuesBPR = [];
                DATA_DISPLAY.logOfDataValuesBPR = [];
            }

            // Show or hide the automatic intensity limits button
            if (badPixelsExist) {
                $('#plotControlAutoImage').show();
            } else {
                $('#plotControlAutoImage').hide();
            }

            // Take the log of the points, save for future use
            for (i = 0; i < value.length; i += 1) {

                DATA_DISPLAY.logOfDataValues[i] = [];
                DATA_DISPLAY.stringDataValues[i] = [];
                if (badPixelsExist) {
                    DATA_DISPLAY.initialDataValuesBPR[i] = [];
                    DATA_DISPLAY.logOfDataValuesBPR[i] = [];
                }

                for (j = 0; j < value[i].length; j += 1) {

                    // If there are any bad pixels, remove them from the image
                    // by setting their values to null
                    if (badPixelsExist) {
                        if (value[i][j] >= bpr_limits[0] &&
                                value[i][j] <= bpr_limits[1]) {

                            DATA_DISPLAY.initialDataValuesBPR[i][j] =
                                value[i][j];

                            // Calculate the log of each value
                            if (value[i][j] > 0) {
                                DATA_DISPLAY.logOfDataValuesBPR[i][j] =
                                    Math.log(value[i][j]) / Math.LN10;
                            } else {
                                // Set log of <= 0 values to null - I think it
                                // looks nicer when plotted
                                DATA_DISPLAY.logOfDataValuesBPR[i][j] = null;
                            }

                        } else {
                            DATA_DISPLAY.initialDataValuesBPR[i][j] = null;
                            DATA_DISPLAY.logOfDataValuesBPR[i][j] = null;
                        }
                    }

                    // Calculate the log of each value
                    if (value[i][j] > 0) {
                        DATA_DISPLAY.logOfDataValues[i][j] =
                            Math.log(value[i][j]) / Math.LN10;
                    } else {
                        // Set log of <= 0 values to null - I think it
                        // looks nicer when plotted
                        DATA_DISPLAY.logOfDataValues[i][j] = null;
                    }

                    // Convert to string for use in the mouse-over tool-tip,
                    // otherwise values of '0' are not displayed and I also
                    // want to have the tool-tip show out of range values even
                    // if they are not displayed on the plot
                    DATA_DISPLAY.stringDataValues[i][j] =
                        value[i][j].toString();
                }
            }

            // Set the data that will be used for plotting
            DATA_DISPLAY.setDataToDisplay();

            if (debug) {
                console.log('*** END data-display.js createImageArrays');
            }
        },

        // Set the data that will be used for plotting - log or not, automatic
        // limits or not
        setDataToDisplay : function () {

            if (DATA_DISPLAY.plotLogValues) {

                if (DATA_DISPLAY.plotAutoLimits &&
                        DATA_DISPLAY.badPixelsExist) {
                    DATA_DISPLAY.dataValues = DATA_DISPLAY.logOfDataValuesBPR;
                } else {
                    DATA_DISPLAY.dataValues = DATA_DISPLAY.logOfDataValues;
                }

            } else {

                if (DATA_DISPLAY.plotAutoLimits &&
                        DATA_DISPLAY.badPixelsExist) {
                    DATA_DISPLAY.dataValues =
                        DATA_DISPLAY.initialDataValuesBPR;
                } else {
                    DATA_DISPLAY.dataValues = DATA_DISPLAY.initialDataValues;
                }

            }
        },


        // Save the image data and the log of the image data to global
        // variables (2D heatmaps have no option to switch the z-axis to log
        // scale!)
        //
        //  - raw image size : shape dims
        //  - loaded image size : width and height in number of pixels
        //  - loaded image range : of the raw image
        //  - zoom range
        //
        //------------------------------------------------
        //  - original data image size (data taken)
        //  - selected range of image (zoom)
        //  - size of returned image (could be downsampled or not)
        //------------------------------------------------
        //
        initializeImageData : function (value, bpr_limits, badPixels,
            is_downsampled) {

            var debug = false;

            DATA_DISPLAY.dataValues = value;

            // The size (in pixels) of the image
            DATA_DISPLAY.loadedImageSize = [DATA_DISPLAY.dataValues[0].length,
                DATA_DISPLAY.dataValues.length];

            // The range of the image
            //      → should be 0 to something, unless zoomed in on a
            //        previously downsampled image
            if (DATA_DISPLAY.imageZoomSection) {
                DATA_DISPLAY.loadedImageRange =
                    DATA_DISPLAY.imageZoomSection;
            } else {
                DATA_DISPLAY.loadedImageRange =
                    [0, DATA_DISPLAY.imageShapeDims[0],
                        0, DATA_DISPLAY.imageShapeDims[1]];
            }

            if (!DATA_DISPLAY.imageZoomSection) {
                DATA_DISPLAY.imageZoomSection = DATA_DISPLAY.loadedImageRange;
            }

            // The size of the image range
            DATA_DISPLAY.loadedImageRangeSize[0] =
                DATA_DISPLAY.loadedImageRange[1] -
                DATA_DISPLAY.loadedImageRange[0];
            DATA_DISPLAY.loadedImageRangeSize[1] =
                DATA_DISPLAY.loadedImageRange[3] -
                DATA_DISPLAY.loadedImageRange[2];

            // Check if the image has been downsampled
            DATA_DISPLAY.imageIsDownsampled = is_downsampled;

            if (debug) {
                console.log('DATA_DISPLAY.imageShapeDims[0]' +
                    DATA_DISPLAY.imageShapeDims[0]);
                console.log('DATA_DISPLAY.imageShapeDims[1]' +
                    DATA_DISPLAY.imageShapeDims[1]);
                console.log('DATA_DISPLAY.loadedImageSize: ' +
                    DATA_DISPLAY.loadedImageSize);
                console.log('DATA_DISPLAY.loadedImageRange: ' +
                    DATA_DISPLAY.loadedImageRange);
                console.log('DATA_DISPLAY.loadedImageRangeSize: ' +
                    DATA_DISPLAY.loadedImageRangeSize);
                console.log('DATA_DISPLAY.imageZoomSection:   ' +
                    DATA_DISPLAY.imageZoomSection);
                console.log('DATA_DISPLAY.imageIsDownsampled: ' +
                    DATA_DISPLAY.imageIsDownsampled);
                console.log('DATA_DISPLAY.usingOriginalImage: ' +
                    DATA_DISPLAY.usingOriginalImage);
            }

            // Create the image arrays
            DATA_DISPLAY.createImageArrays(value, bpr_limits, badPixels);

            if (debug) {
                console.log('DATA_DISPLAY.initialDataValues:');
                console.log(DATA_DISPLAY.initialDataValues);
            }
        },


        // Check if the object is a number
        isNumeric : function (n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        },

        // Check if odd or even
        isEven : function (n) {

            var result = (n % 2 === 0) ? true : false;
            return result;
        },

        // Save some information about an image
        //
        // image
        //      - new image
        //      - decimated image
        //      - zoomed
        //          - fetched zoomed area
        //          -
        //
        // image series
        //      - new series
        //      - new image in same series
        //          - previous image was zoomed
        //      - zoomed
        //          - fetched zoomed area
        //
        //------------------------------------------------
        //  - original data image size (data taken)
        //  - selected range of image (zoom)
        //  - size of returned image (could be downsampled or not)
        //------------------------------------------------
        //
        saveImageInfo : function (targetUrl, shapeDims, newImage, section,
            imageTitle) {

            var debug = false;

            // Save some data, if provided
            if (targetUrl) {
                DATA_DISPLAY.imageTargetUrl = targetUrl;
            }

            if (shapeDims) {

                // image-series
                if (shapeDims.length === 3) {
                    DATA_DISPLAY.imageSeriesRange = shapeDims[0];

                    // Switch (row,col) notation to (x,y) notation
                    DATA_DISPLAY.imageShapeDims = [shapeDims[2], shapeDims[1]];

                    if (section) {
                        DATA_DISPLAY.imageSeriesIndex = section[0];
                    } else {
                        DATA_DISPLAY.imageSeriesIndex = 0;
                    }

                // image
                } else {
                    DATA_DISPLAY.imageSeriesRange = 1;
                    DATA_DISPLAY.imageSeriesIndex = false;

                    // Switch (row,col) notation to (x,y) notation
                    DATA_DISPLAY.imageShapeDims = [shapeDims[1], shapeDims[0]];
                }
            }

            DATA_DISPLAY.usingOriginalImage = newImage;

            // Save zooming info
            if (section !== true) {
                DATA_DISPLAY.imageZoomSection = section;
            }

            if (section) {

                // image-series
                if (shapeDims.length === 3) {
                    DATA_DISPLAY.imageZoomSection = [];
                    DATA_DISPLAY.imageZoomSection[0] = section[2];
                    DATA_DISPLAY.imageZoomSection[1] = section[3];
                    DATA_DISPLAY.imageZoomSection[2] = section[4];
                    DATA_DISPLAY.imageZoomSection[3] = section[5];

                // image
                } else {
                    DATA_DISPLAY.imageZoomSection = section;
                }
            }


            // Set the plot title
            if (imageTitle) {
                DATA_DISPLAY.imageTitle = imageTitle;
            }

            if (debug) {
                console.log('DATA_DISPLAY.imageTitle: ' +
                    DATA_DISPLAY.imageTitle);
                console.log('DATA_DISPLAY.imageSeriesIndex: ' +
                    DATA_DISPLAY.imageSeriesIndex);
                console.log('DATA_DISPLAY.imageSeriesRange: ' +
                    DATA_DISPLAY.imageSeriesRange);
                console.log('DATA_DISPLAY.imageShapeDims[0]: ' +
                    DATA_DISPLAY.imageShapeDims[0]);
                console.log('DATA_DISPLAY.imageShapeDims[1]: ' +
                    DATA_DISPLAY.imageShapeDims[1]);
                console.log('DATA_DISPLAY.imageZoomSection:   ' +
                    DATA_DISPLAY.imageZoomSection);
                console.log('DATA_DISPLAY.usingOriginalImage: ' +
                    DATA_DISPLAY.usingOriginalImage);
                console.log('** DATA_DISPLAY.saveImageInfo done **');
            }

        },


    };


// This function fires when the browser window is resized
$(window).resize(function () {

    var debug = false;

    if (debug) {
        console.log('wait for it...');
    }

    DATA_DISPLAY.mobileDisplay = window.mobilecheck();
    DATA_DISPLAY.redrawPlotCanvas(100);
});


// This function fires when the page is ready
$(document).ready(function () {

    var debug = false;

    if (debug) {
        console.log('document is ready');
    }

    // Calculate the proper plot dimensions and save them
    DATA_DISPLAY.calculatePlotSize();

});

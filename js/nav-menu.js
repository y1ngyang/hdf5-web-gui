/*global $*/
'use strict';


// The global variables for this applicaiton
var DATA_DISPLAY, AJAX_SPINNER,

    NAV_MENU = {

        "navMenuWidth" : 0,
        "navMenuWidthChanged" : false,
        "displayOffsetWidth" : "0px",
        "loaderOffsetWidth" : "55px",

        "menuIsOpen" : false,
        "displayContainer" : "displayContainer",
        "loaderContainer" : "loader",
        "sideNavMenu" : "side-nav-menu",
        "mobileView" : undefined,


        // If the menu button is pressed
        menuButton : function () {

            var debug = false;

            if (debug) {
                console.log("menuButton() called");
                console.log("NAV_MENU.menuIsOpen:   " + NAV_MENU.menuIsOpen);
            }

            // If the nav menu is open then close it, if it's closed, open it.
            if (NAV_MENU.menuIsOpen) {
                NAV_MENU.closeNav();
            } else {
                NAV_MENU.openNav();
            }

            NAV_MENU.setPlotAreaWidth();

        },


        // Open the side menu
        openNav : function () {

            // Slide the menu out
            document.getElementById("side-nav-menu").style.display =
                "inline-block";

            NAV_MENU.menuIsOpen = true;
        },


        // Close the side menu
        closeNav : function () {

            // Hide the menu
            document.getElementById("side-nav-menu").style.display = "none";

            NAV_MENU.menuIsOpen = false;
        },


        // Resize the plotting area if needed when the navigation menu width
        // changes
        navMenuWidthChange : function () {

            var debug = false;

            if (debug) {
                console.log('navMenuWidthChange() called');
            }

            NAV_MENU.setPlotAreaWidth();
        },


        measureNavMenuWidth : function () {

            var debug = false, presentWidth;

            presentWidth =
                document.getElementById("side-nav-menu").offsetWidth;

            if (debug) {
                console.log("  NAV_MENU.navMenuWidth:       " +
                    NAV_MENU.navMenuWidth);
            }

            // If the widht has changed by a certain amount, note the change
            if (Math.abs(presentWidth - NAV_MENU.navMenuWidth) > 10) {
                NAV_MENU.navMenuWidth = presentWidth;
                NAV_MENU.navMenuWidthChanged = true;
            } else {
                NAV_MENU.navMenuWidthChanged = false;
            }

            if (NAV_MENU.navMenuWidth > 0) {
                NAV_MENU.menuIsOpen = true;
            } else {
                NAV_MENU.menuIsOpen = false;
            }

            if (debug) {
                console.log("  presentWidth:                " + presentWidth);
                console.log("  NAV_MENU.navMenuWidth:       " +
                    NAV_MENU.navMenuWidth);
                console.log("  NAV_MENU.menuIsOpen:         " +
                    NAV_MENU.menuIsOpen);
                console.log("  NAV_MENU.navMenuWidthChanged:" +
                    NAV_MENU.navMenuWidthChanged);
            }

        },


        // Set the plotting area width after the the navigation menu width has
        // changed
        setPlotAreaWidth : function () {

            var debug = false;

            NAV_MENU.measureNavMenuWidth();

            if (NAV_MENU.navMenuWidthChanged) {

                NAV_MENU.mobileView = window.mobilecheck();

                if (!NAV_MENU.mobileView) {


                    // The new width of the plotting area
                    NAV_MENU.displayOffsetWidth =
                        (NAV_MENU.navMenuWidth + 5) + "px";

                    // Center the loading icon too
                    // NAV_MENU.loaderOffsetWidth =
                    //     (NAV_MENU.navMenuWidth - 75) + "px";

                    if (debug) {
                        console.log("NAV_MENU.displayOffsetWidth: " +
                            NAV_MENU.displayOffsetWidth);
                        console.log("NAV_MENU.loaderOffsetWidth:  " +
                            NAV_MENU.loaderOffsetWidth);
                    }

                    // Slide page content out so that it is not hidden by the
                    // menu
                    document.getElementById(NAV_MENU.displayContainer
                        ).style.marginLeft = NAV_MENU.displayOffsetWidth;
                    document.getElementById(NAV_MENU.loaderContainer
                        ).style.marginLeft = NAV_MENU.loaderOffsetWidth;

                    // Redraw the plot, waiting a bit for nav menu animation
                    DATA_DISPLAY.redrawPlotCanvas(0);

                }
            }
        },

    };

/*global $*/
'use strict';


// External libraries
var SERVER_COMMUNICATION, AJAX_SPINNER, HANDLE_DATASET, DATA_DISPLAY,
    CONFIG_DATA, NAV_MENU, EventSource,

    // Some gloabl variables
    FILE_NAV =
    {
        jstreeArray : [],
        processSelectNodeEvent : true,
        idsToReload : [],
        data : null,
        useDarkTheme : false,
        temp : false,


        // Error message - empty file, folder, no user access
        displayErrorMessage : function (objectTitle, message) {

            // Display the message
            DATA_DISPLAY.drawText(objectTitle, message, '#ad3a3a');
        },


        // Add new item to the file browser tree
        addToTree : function (itemList, selectedId, createNewTree) {

            var debug = false, keyTitle = '', icon = '', doesNodeExist = false,
                needToRefresh = false;

            if (createNewTree) {
                FILE_NAV.jstreeArray = [];
            }

            if (itemList === false) {
                if (debug) {
                    console.log('folder contents are empty');
                }
            }

            // Loop over the list of items
            for (keyTitle in itemList) {
                if (itemList.hasOwnProperty(keyTitle)) {

                    if (debug) {
                        console.log(keyTitle + " -> url:        " +
                            itemList[keyTitle].url);
                        console.log(keyTitle + " -> item_type:  " +
                            itemList[keyTitle].item_type);
                        console.log(keyTitle + " -> full_name:  " +
                            itemList[keyTitle].full_name);
                        console.log(keyTitle + " -> short_name: " +
                            itemList[keyTitle].short_name);
                        console.log(keyTitle + " -> readable:   " +
                            itemList[keyTitle].readable);
                    }

                    // Choose an icon for the tree for this item
                    if (itemList[keyTitle].item_type) {

                        switch (itemList[keyTitle].item_type) {
                        case 'folder':
                        case 'h5_folder':
                            icon = 'fa fa-folder';
                            break;
                        case 'h5_file':
                            icon = '../images/hdf5-16px.png';
                            break;
                        case 'image-series':
                            icon = 'fa fa-stack-overflow';
                            break;
                        case 'image':
                            icon = 'fa fa-area-chart';
                            break;
                        case 'line':
                            icon = 'fa fa-line-chart';
                            break;
                        case 'number':
                            // icon = 'fa fa-barcode';
                            icon = 'fa fa-slack';
                            break;
                        case 'text':
                            // icon = 'fa fa-list';
                            icon = 'fa fa-comment';
                            break;
                        default:
                            icon = 'fa fa-question-circle';
                        }

                    } else {
                        icon = 'fa fa-minus-circle';
                    }

                    // Check if this id exists already in the tree
                    doesNodeExist = false;
                    if (!createNewTree) {
                        doesNodeExist = $('#jstree_div').jstree(true).get_node(
                            itemList[keyTitle].full_name
                        );

                        if (debug) {
                            console.log('doesNodeExist: ');
                            console.log(doesNodeExist);
                        }
                    }

                    // If this has not already been added to the tree and it is
                    // readable by the user, add it to the tree
                    if (!doesNodeExist && itemList[keyTitle].readable) {

                        FILE_NAV.jstreeArray.push({

                            // The specific key-value pairs needed by jstree
                            "id" : itemList[keyTitle].full_name,
                            "parent" : (selectedId === false ? '#' :
                                    selectedId),
                            "text" : itemList[keyTitle].short_name,
                            "icon" : icon,

                            // Save some additional information - is this a
                            // good place to put it?
                            "data" : {
                                "item_type" : itemList[keyTitle].item_type,
                                "url" : itemList[keyTitle].url,
                                "full_name" : itemList[keyTitle].full_name,
                                "short_name" : itemList[keyTitle].short_name,
                                "readable" : itemList[keyTitle].readable,
                            },

                            state : {
                                checkbox_disabled : true
                            },
                        });

                        needToRefresh = true;
                    }
                }
            }

            // Create or add to the jstree object
            if (createNewTree) {

                $('#jstree_div').jstree(
                    {
                        "core" : {
                            "data" : FILE_NAV.jstreeArray,
                            "themes" : {
                                "name" : (FILE_NAV.useDarkTheme === true ?
                                        "default-dark" : "default"),
                                "dots" : true,
                                "icons" : true,

                                // "responsive" : true,
                                "ellipsis" : true,
                            },

                            "expand_selected_onload" : true,
                        },

                        "plugins": ["sort"],
                    }
                );

            } else {

                if (debug) {
                    console.log('needToRefresh: ' + needToRefresh);
                }

                if (needToRefresh) {
                    FILE_NAV.processSelectNodeEvent = false;
                    $('#jstree_div').jstree(true).settings.core.data =
                        FILE_NAV.jstreeArray;
                    $('#jstree_div').jstree(true).refresh(selectedId);
                }
            }

            if (debug) {
                console.table(FILE_NAV.jstreeArray, ["text", "id"]);
            }

        },


        // Given a url, make a list of all the contents (files, folders,
        // datasets) saving some information about each one.
        getFolderContents : function (linksUrl, selectedId, createNewTree) {

            var debug = false;

            return $.when(SERVER_COMMUNICATION.ajaxRequest(linksUrl)).then(
                function (response) {

                    var folder_key, folder_item,
                        folder_item_key, folder_item_info;

                    // Look for the 'folder_contents' section
                    if (response.hasOwnProperty('folder_contents')) {

                        if (debug) {

                            // Loop over each item (folder, file, or dataset)
                            for (folder_key in response.folder_contents) {
                                if (response.folder_contents.hasOwnProperty(
                                        folder_key
                                    )) {

                                    folder_item =
                                        response.folder_contents[folder_key];

                                    console.log('folder_key: ' + folder_key);

                                    // Loop over each attribute of an item,
                                    // name, type, etc.
                                    for (folder_item_key in folder_item) {
                                        if (folder_item.hasOwnProperty(
                                                folder_item_key
                                            )) {

                                            folder_item_info =
                                                folder_item[folder_item_key];

                                            console.log('  ' + folder_item_key
                                                + " -> " + folder_item_info);
                                        }
                                    }

                                }
                            }

                        }

                        if (debug) {
                            console.log(response.folder_contents);
                        }

                        // Update the jstree object
                        if (response.folder_contents !== false &&
                                !$.isEmptyObject(response.folder_contents)) {

                            FILE_NAV.addToTree(response.folder_contents,
                                selectedId, createNewTree);
                        } else {

                            if ($.isEmptyObject(response.folder_contents)) {

                                if (debug) {
                                    console.log('folder contents are empty');
                                }

                                FILE_NAV.displayErrorMessage(selectedId,
                                    'Is empty!');
                            } else {
                                FILE_NAV.displayErrorMessage(selectedId,
                                    'Is not accessible!');
                            }
                        }

                    }

                    return true;
                }
            );
        },


        // Get a list of items in the root directory, then create the jstree
        // object
        getRootDirectoryContents : function () {

            var debug = false, initialUrl = CONFIG_DATA.hdf5DataServer;

            if (debug) {
                console.log('initialUrl: ' + initialUrl);
            }

            return $.when(FILE_NAV.getFolderContents(initialUrl,
                false, true)).then(
                function () {

                    if (debug) {
                        console.log('getFolderContents done');
                    }

                    return true;
                }
            );

        },


        // Set the height of the div containing the file browsing tree
        setTreeDivHeight : function () {

            var window_height = $(window).height(),
                content_height = window_height - 80;

            $('#treeSectionDiv').height(content_height);

        },


        changeFolderIcon : function (eventInfo, data, openFolder) {

            var debug = false;

            if (debug) {
                console.log(eventInfo);
                console.log(data);
                console.log(openFolder);
            }

            if (data.node.data.item_type === 'folder') {
                if (openFolder) {
                    data.instance.set_icon(data.node, 'fa fa-folder-open');
                } else {
                    data.instance.set_icon(data.node, 'fa fa-folder');
                }
            }

        },


        // When an item in the tree is clicked, do something - open a folder,
        // file, display an image, etc.
        treeItemClickedOptions : function (data) {

            var debug = false, dataTitle, dataTarget;

            dataTarget = data.node.data.url;

            if (debug) {
                console.log(data);
                console.log(dataTarget);
            }

            // Do different things depending on what type of item has been
            // clicked
            if (data.node.data.item_type === 'folder' ||
                    data.node.data.item_type === 'h5_folder' ||
                    data.node.data.item_type === 'h5_file') {

                // FILE_NAV.getFolderContents(dataTarget, data.selected,
                // false);

                $.when(FILE_NAV.getFolderContents(dataTarget, data.selected,
                        false)).then(
                    function () {
                        if (debug) {
                            console.log('Done getting stuff');
                        }

                        setTimeout(function () {
                            NAV_MENU.navMenuWidthChange();
                        }, 100);
                    }
                );


            } else {

                // Empty the plot canvas, get ready for some new stuff
                DATA_DISPLAY.purgePlotCanvas();
                DATA_DISPLAY.enableImagePlotControls(false, false);

                dataTitle = data.node.data.short_name;

                switch (data.node.data.item_type) {

                case 'image-series':
                case 'image':
                    AJAX_SPINNER.startLoadingData(10);
                    HANDLE_DATASET.displayImage(dataTarget, false, true,
                        dataTitle);
                    break;

                case 'line':
                    AJAX_SPINNER.startLoadingData(10);
                    HANDLE_DATASET.displayLine(dataTarget, data.selected,
                        dataTitle);
                    break;

                case 'number':
                    HANDLE_DATASET.displayText(dataTarget, dataTitle,
                        '#ad3a3a');
                    break;

                case 'text':
                    HANDLE_DATASET.displayText(dataTarget, dataTitle,
                        '#3a74ad');
                    break;

                default:
                    if (debug) {
                        console.log('Is this a fucking dataset? No!');
                    }
                    DATA_DISPLAY.displayErrorMessage(dataTarget);
                }
            }

        },


        // When an item in the tree is clicked, do something - open a folder,
        // file, display an image, etc.
        treeItemClicked : function (eventInfo, data) {

            var debug = false;

            if (debug) {
                console.log('FILE_NAV.processSelectNodeEvent: ' +
                    FILE_NAV.processSelectNodeEvent);
            }

            // Open or close the node graphically
            $('#jstree_div').jstree(true).toggle_node(data.node.id);

            if (debug) {
                console.log('');
                console.log('* tree node event');
                console.log(eventInfo);
                console.log('');
                console.log('* tree node data');
                console.log(data);
            }

            if (FILE_NAV.processSelectNodeEvent) {
                FILE_NAV.treeItemClickedOptions(data);
            } else {
                if (debug) {
                    console.log('tree item selected, didn\'t do shit though');
                }

                // Reset some global variables
                FILE_NAV.processSelectNodeEvent = true;
            }

            if (debug) {
                console.log('FILE_NAV.processSelectNodeEvent: ' +
                    FILE_NAV.processSelectNodeEvent);
            }
        },

    };


// Change the icon when a folder is opened
$("#jstree_div").on('open_node.jstree', function (eventInfo, data) {
    // console.log('open_node.jstree');
    FILE_NAV.changeFolderIcon(eventInfo, data, true);

    // Delay before checking nav menu width - give time to possible reading of
    // folder contents
    setTimeout(function () {
        NAV_MENU.navMenuWidthChange();
    }, 400);

// Change the icon when a folder is closed
}).on('close_node.jstree', function (eventInfo, data) {
    // console.log('close_node.jstree');
    FILE_NAV.changeFolderIcon(eventInfo, data, false);

    // Delay before checking nav menu width - give time to possible reading of
    // folder contents
    setTimeout(function () {
        NAV_MENU.navMenuWidthChange();
    }, 400);
});


// When an item in the tree is clicked, do some stuff
$('#jstree_div').on("select_node.jstree", function (eventInfo, data) {
    // console.log('select_node.jstree');
    FILE_NAV.treeItemClicked(eventInfo, data);
});


// This function fires when the browser window is resized
$(window).resize(function () {
    FILE_NAV.setTreeDivHeight();
});


// This function fires when the page is ready
$(document).ready(function () {

    var debug = false;

    if (debug) {
        console.log('file-navigation.js - document is ready');
    }

    // Set the height of the div containing the file browsing tree
    FILE_NAV.setTreeDivHeight();

});

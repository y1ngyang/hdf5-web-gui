/*global $*/
'use strict';

// External libraries
var SERVER_COMMUNICATION, DATA_DISPLAY, FILE_NAV, AJAX_SPINNER,

    // The gloabl variables for this applicaiton
    HANDLE_DATASET =
    {

        // Return a dataset value
        getDatasetValue : function (inputUrl) {

            var debug = false, valueUrl;


            if (debug) {
                console.log('inputUrl: ' + inputUrl);
            }

            valueUrl = inputUrl.replace('?host', '/value?host');

            if (debug) {
                console.log('valueUrl: ' + valueUrl);
            }


            // Get the data
            return $.when(SERVER_COMMUNICATION.ajaxRequest(valueUrl)).then(
                function (response) {
                    return response.value;
                }
            );
        },


        // When an image is selected, get it and plot it
        displayImage : function (inputUrl, section, newImage, imageTitle) {

            var debug = false;

            if (debug) {
                console.log('** HANDLE_DATASET.displayImage entered **');
            }

            // Hide the 'Reset Zoom' button if this is a new image selected
            // from the tree menu
            if (newImage === true && section === false) {
                $('#plotControlReset').hide();
            }

            // Get the image along with some information about it
            return $.when(HANDLE_DATASET.getImage(inputUrl, section)).then(

                function (response) {

                    var isImageSeries = false, values, shape, bpr_limits;

                    if (debug) {
                        console.log('HANDLE_DATASET.displayImage response ' +
                            'returned');
                        console.log(response);
                    }

                    if (response.hasOwnProperty('readable')) {

                        if (response.readable) {

                            // Double check that certain items are indeed
                            // arrays, if not, assume that they are strings
                            // containing array notation, and parse them
                            values = response.values;
                            section = response.section;
                            bpr_limits = response.bpr_limits;

                            shape = (response.shape instanceof Array ?
                                    response.shape :
                                    JSON.parse(response.shape));
                            values = (response.values instanceof Array ?
                                    response.values :
                                    JSON.parse(response.values));
                            section = (response.section instanceof Array ?
                                    response.section :
                                    JSON.parse(response.section));
                            bpr_limits = (
                                response.bpr_limits instanceof Array ?
                                        response.bpr_limits :
                                        JSON.parse(response.bpr_limits)
                            );

                            isImageSeries = (
                                response.data_type === 'image-series' ?
                                        true : false
                            );

                            // Switch (row,col) notation to (x,y) notation
                            if (isImageSeries) {
                                section = [section[0], section[1], section[4],
                                    section[5], section[2], section[3]];
                            } else {
                                section = [section[2], section[3], section[0],
                                    section[1]];
                            }

                            // Save some information about the image
                            DATA_DISPLAY.saveImageInfo(inputUrl,
                                shape, newImage, section, imageTitle);

                            DATA_DISPLAY.initializeImageData(values,
                                bpr_limits,
                                response.bad_pixels_exist,
                                response.is_downsampled);

                            if (debug) {
                                console.log('HANDLE_DATASET.displayImage ' +
                                    'newImage: ' + newImage);
                            }

                            // For zooming in a large downsampled image, this
                            // should be false
                            if (newImage) {

                                // Enable plot controls
                                DATA_DISPLAY.enableImagePlotControls(true,
                                    true, isImageSeries);

                                // Plot the data
                                DATA_DISPLAY.plotData();
                            }
                        } else {
                            if (debug) {
                                console.log('object cannot be read');
                            }
                            FILE_NAV.displayErrorMessage(imageTitle,
                                'Object cannot be read!');
                        }

                    } else {
                        if (debug) {
                            console.log('object cannot be read');
                        }
                        FILE_NAV.displayErrorMessage(imageTitle,
                            'Object cannot be read!');
                    }

                    if (debug) {
                        console.log(
                            '** HANDLE_DATASET.displayImage finished **'
                        );
                    }
                    return true;
                }
            );
        },


        // When a dataset is selected, plot the data
        getImage : function (inputUrl, section) {

            var debug = false, valueUrl = inputUrl;

            if (debug) {
                console.log('** HANDLE_DATASET.getImage entered **');
            }

            // Images are stored in (row, col) notation, which corresponds to
            // (y, x), but zooming in the plotly plots gives corrdinates in the
            // (x, y) notation, so they are flipped here in the slice creation
            if (section) {

                // images
                if (section.length === 4) {
                    // axes labels are flipped (I think) in hdf5 datasets
                    valueUrl += '[' + section[2] + ':' + section[3] + ','
                        + section[0] + ':' + section[1] + ']';
                }

                // image series
                if (section.length === 6) {
                    // axes labels are flipped (I think) in hdf5 datasets
                    valueUrl += '[' + section[0] + ':' + section[1] + ','
                        + section[4] + ':' + section[5] + ',' +
                        section[2] + ':' + section[3] + ']';
                }

            }

            if (debug) {
                console.log('HANDLE_DATASET.getImage inputUrl: ' + inputUrl);
                console.log('HANDLE_DATASET.getImage section:  ' + section);
                console.log('HANDLE_DATASET.getImage valueUrl: ' + valueUrl);
            }

            // Get the data
            return $.when(SERVER_COMMUNICATION.ajaxRequest(valueUrl)).then(
                function (response) {

                    if (debug) {
                        console.log('HANDLE_DATASET.getImage response');
                        console.log(response);
                    }

                    if (debug) {
                        console.log('** HANDLE_DATASET.getImage returning **');
                    }
                    return response;
                }
            );
        },


        // Get a single image from a stack of images, which are typically
        // saved as 3 dimensional arrays, with the first dimension being
        // the image number
        readImageFromSeries : function (targetUrl, imageIndex, section) {

            var debug = false, valueUrl, chunks, matrix, numChunkRows,
                numChunkColumns, imageIndexStart, imageIndexStop;

            // The selected image in the stack is just a single slice of a
            // python array
            imageIndexStart = Number(imageIndex);
            imageIndexStop = Number(imageIndexStart + 1);

            if (debug) {
                console.log('targetUrl: ' + targetUrl);
            }

            valueUrl = targetUrl.replace('?host', '/value?host') +
                '[' + imageIndexStart + ':' + imageIndexStop + ',';

            if (section) {
                valueUrl += section[2] + ':' + section[3] + ','
                    + section[0] + ':' + section[1] + ']';
            } else {
                valueUrl += ':,:]';
            }

            if (debug) {
                console.log('valueUrl: ' + valueUrl);
            }

            // Get the image data - actually a 3D array
            return $.when(SERVER_COMMUNICATION.ajaxRequest(valueUrl)).then(
                function (response) {

                    // This should be a 3D array
                    chunks = response.value;

                    // Each chunk should be a matrix (2D array)
                    matrix = chunks[0];

                    if (debug) {
                        numChunkRows = matrix.length;
                        numChunkColumns = matrix[0].length;
                        console.log('numChunkRows:     ' + numChunkRows);
                        console.log('numChunkColumns : ' +
                            numChunkColumns);
                    }

                    // Return the 2D array === image
                    return matrix;
                }
            );

        },


        imageSeriesStep : function (stepUp) {

            var debug = false, imageIndex = 0;

            // Need to add a check to see if an image series is being displayed

            if (debug) {
                console.log('DATA_DISPLAY.imageSeriesIndex: ' +
                    DATA_DISPLAY.imageSeriesIndex);
            }

            if (stepUp) {
                imageIndex = Number(DATA_DISPLAY.imageSeriesIndex) + 1;
            } else {
                imageIndex = Number(DATA_DISPLAY.imageSeriesIndex) - 1;
            }

            if (debug) {
                console.log('imageIndex: ' + imageIndex);
            }

            HANDLE_DATASET.imageSeriesInput(imageIndex);

        },


        // Handle input from image series control buttons
        // This assumes that displayImageSeriesInitial() has at some point
        // already been called
        imageSeriesInput : function (imageIndex, zoomSection) {

            var debug = false, section, min = 0,
                max = DATA_DISPLAY.imageSeriesRange - 1;

            if (debug) {
                console.log('** HANDLE-DATASET.imageSeriesInput **');
                console.log('  imageIndex:  ' + imageIndex);
                console.log('  zoomSection: ' + zoomSection);
            }

            // Check if input was numeric
            if (DATA_DISPLAY.isNumeric(imageIndex)) {

                // Check for out of range values
                if (imageIndex < min) {
                    imageIndex = min;
                }

                if (imageIndex > max) {
                    imageIndex = max;
                }

                // The zoomed-in section, if applicable
                if (zoomSection === undefined || zoomSection === true) {
                    section = [imageIndex, imageIndex + 1,
                        DATA_DISPLAY.imageZoomSection[0],
                        DATA_DISPLAY.imageZoomSection[1],
                        DATA_DISPLAY.imageZoomSection[2],
                        DATA_DISPLAY.imageZoomSection[3]];
                } else if (zoomSection === false) {
                    section = false;
                    section = [imageIndex, imageIndex + 1,
                        0, DATA_DISPLAY.imageShapeDims[0],
                        0, DATA_DISPLAY.imageShapeDims[1]];
                } else {
                    DATA_DISPLAY.imageZoomSection = zoomSection;
                    section = [imageIndex, imageIndex + 1,
                        zoomSection[0],
                        zoomSection[1],
                        zoomSection[2],
                        zoomSection[3]];
                }

                if (debug) {
                    console.log('imageIndex: ' + imageIndex);
                    console.log('section: ');
                    console.log(section);
                    console.log('min: ' + min);
                    console.log('max: ' + max);
                }

                // Start the spinner
                AJAX_SPINNER.startLoadingData(1);

                // Set image series entry field value
                $("#inputNumberDiv").val(imageIndex);
                $("#imageSeriesSlider").val(imageIndex);

                // Get an image from the series and display it
                HANDLE_DATASET.displayImage(DATA_DISPLAY.imageTargetUrl,
                    section, true, DATA_DISPLAY.imageTitle);

            } else {

                // If the given image index was not numeric, return to the
                // beginning of the series
                HANDLE_DATASET.imageSeriesInput(0);
            }
        },


        // Get a simple data value - text or a number
        getDataValue : function (dataUrl, getItem) {

            var debug = false, returnValue = '';

            return $.when(SERVER_COMMUNICATION.ajaxRequest(dataUrl)).then(
                function (response) {

                    var key = '';

                    if (debug) {
                        for (key in response) {
                            if (response.hasOwnProperty(key)) {
                                console.log(key + " -> " + response[key]);
                            }
                        }
                    }

                    if (response.hasOwnProperty(getItem)) {
                        returnValue = response[getItem];
                    }

                    return returnValue;
                }
            );

        },


        // When a dataset is selected, display whatever text there is
        displayText : function (inputUrl, itemTitle, fontColor) {

            var debug = false;

            if (debug) {
                console.log('inputUrl: ' + inputUrl);
            }

            // Get the data
            $.when(HANDLE_DATASET.getDataValue(inputUrl, 'values')).then(
                function (values) {

                    if (debug) {
                        console.log('values:  ' + values);
                    }

                    // Display the data
                    DATA_DISPLAY.drawText(itemTitle, values, fontColor);
                }
            );

        },

        displayLine : function (inputUrl, imageTitle) {

            var debug = false;

            if (debug) {
                console.log('inputUrl: ' + inputUrl);
            }

            // Hide the 'Reset Zoom' button, assuming this is a new image
            // selected from the tree menu
            $('#plotControlReset').hide();

            // Get the data (from data-retrieval.js), then plot it
            $.when(SERVER_COMMUNICATION.ajaxRequest(inputUrl)).then(
                function (response) {

                    if (debug) {
                        console.log(response);
                    }

                    // Enable some plot controls
                    DATA_DISPLAY.enableImagePlotControls(true, false, false);

                    // Display the data
                    DATA_DISPLAY.plotLine(response.values, imageTitle);
                }
            );

        },



    };

/*global $*/
'use strict';

// External libraries
var CONFIG_DATA,

    CAS_LOGIN_LOGOUT = {

        // Log out of the application and CAS
        logout : function () {

            var debug = false, redirectUrl;

            redirectUrl = CONFIG_DATA.hdf5DataServer + '/logout';

            if (debug) {
                console.log('Redirecting to ' + redirectUrl);
            }

            window.location = redirectUrl;
        },

    };

# HDF5 WEB GUI

## OVERVIEW

This is a web GUI for viewing HDF5 files.

The HDF Group  [made their own web ui](http://data.hdfgroup.org/),
which was the inspiration for this project, but I wanted to use different tools
for the interface than what they had chosen, namely to make something
responsive and to use a different plotting library.

This project makes use of
[HDF5 Simple Server](https://gitlab.com/MAXIV-HDF5/hdf5-web-gui) for the
backend.

This web GUI is written in javascript, sprinkled with a bit of jquery, and
makes use of the [plotly](https://plot.ly/javascript/) graphing libraries.

### SCREENSHOTS

The HDF5 Web GUI, browsing data, viewing an image in a laptop web browser:

![3D surface plot](screenshots/screenshot-entire-gui-2.png)

3-dimensional, rotatable, zoomable plots of data can be made. Here's a
screenshot:

![3D surface plot](screenshots/screenshot-3d-plot.png)

Here's a screenshot of a 2-dimensional zoomable contour plot from a series of
images, where hot pixels have been automatically supressed, and with x and y
profile histograms that match the displayed image:

![2D density plot](screenshots/screenshot-masked-downsampled.png)

And here is a view the application when displayed on a mobile device, the hidden
menu has been clicked, which can then be hidden to just display the plot:

![Mobile view](screenshots/screenshot-mobile-view.png)

## INSTALLATION
Installing this web application can be done on a server which already has an
existing web server running, or it can be installed via the supplied docker
configuration files, which use an nginx web server.

## INSTALLATION WITH DOCKER
Git code from within MAX IV:
```bash
git clone git@gitlab.maxiv.lu.se:web-gui-applications/hdf5-web-gui.git
```
From public gitlab:
```bash
git clone git@gitlab.com:MAXIV-HDF5/hdf5-web-gui.git
```

Edit configuration. just the lines shown below (probably). Note that the
locations of servicePath is inside of the app/ directory inside the container.
```bash
cd hdf5-web-gui/

vim config/config.json
    "hdf5DataServer": "https://hdf5view.maxiv.lu.se:8081",
    "servicePath": "/html/",

vim docker/nginx.conf
	server_name hdf5view.maxiv.lu.se;

vim docker/docker-compose.yml
      config:
        - subnet: 172.23.0.0/16

    volumes:
      - /etc/ssl/localcerts/w-v-hdf5view-0_maxiv_lu_se.crt:/app/certs/ssl_cert.crt:ro
      - /etc/ssl/localcerts/w-v-hdf5view-0_maxiv_lu_se.key:/app/certs/ssl_cert.key:ro
    hostname: hdf5view.maxiv.lu.se
    ports:
      - "8443:8443"
```

And if you want to redirect all http traffic to https, add a section to the
nginx configuration:
```bash
vim docker/nginx.conf
    server {
           listen         8080;
           server_name    hdf5view.maxiv.lu.se;
           return         301 https://$server_name$request_uri;
    }

vim docker/docker-compose.yml
    ports:
      - "80:8080"
```

Build
```bash
docker-compose -f docker/docker-compose.yml build
```

Start up
```bash
docker-compose -f docker/docker-compose.yml up -d
```

Look at logs
```bash
docker-compose -f docker/docker-compose.yml logs -ft
```

Stop
```bash
docker-compose -f docker/docker-compose.yml down
```

Go into the container
```bash
docker exec -it hdf5-web-gui bash
```

View output in web browser
```bash
https://hdf5view.maxiv.lu.se:8443
```
## INSTALLATION WITH EXISTING WEB SERVER

### GIT THE CODE
Within MAX IV:
```bash
git clone git@gitlab.maxiv.lu.se:web-gui-applications/hdf5-web-gui.git
```

From public gitlab:
```bash
git clone git@gitlab.com:MAXIV-HDF5/hdf5-web-gui.git
```

### EDIT CONFIGURATION
Make sure the server name and port is correct, along with the server and port
for the web application.
```bash
vim config/config.json
    "hdf5DataServer": "https://hdf5view.maxiv.lu.se:8081",
    "servicePath": "/hdf5-web-gui/html/",
```
The servers used for hdf5DataServer and serviceUrl do not need to be the same.

## VIEW OUTPUT
```bash
https://myhostname.maxiv.lu.se/hdf5-web-gui/html/app.html
```
